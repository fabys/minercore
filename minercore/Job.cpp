#include "Job.hpp"

Job * Job::FromParams(nlohmann::json params) {
	std::string id = params[0];
	std::string version = params[1];
	std::string prevHash = params[2];
	std::string merklerRoot = params[3];
	std::string time = params[5];
	std::string bits = params[6];
	bool cleanJobs = params[7];

	Job *job = new Job();
	job->_id = id;
	job->_version = version;
	job->_prevHash = prevHash;
	job->_merklerRoot = merklerRoot;
	job->_time = time;
	job->_bits = bits;
	job->_cleanJobs = cleanJobs;

	return job;
}
