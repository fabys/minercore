#pragma once

#include <iostream>
#include <bitset>
#include "Job.hpp"


class CPUSolver
{
private:
	std::string _nonce1;

public:
	CPUSolver();
	void SetNonce(std::string nonce);
	void SetTarget(std::string target);
	void Run(Job *job);
};