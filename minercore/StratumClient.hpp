#pragma once

#include <iostream>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/thread/thread.hpp>
#include "json.hpp"

using namespace std;
using namespace boost::asio;
using boost::asio::ip::tcp;

class StratumClient
{
private:
	boost::asio::io_service _io_service;
	boost::asio::io_service::work work{_io_service};

	tcp::socket _socket;
	bool _connected;
	boost::asio::streambuf _responseBuffer;
	std::function<void(nlohmann::json)> _readCallback;
	std::function<void()> _connectedCallback;
	boost::thread_group _workerThreads;

public:
	StratumClient();
	~StratumClient();

	void Connect(std::string const& host, std::string const& port, std::function<void()> callback);
	nlohmann::json Subscribe();
	nlohmann::json Authorize(std::string const& username, std::string const& password);
	void Start(std::function<void(nlohmann::json)> callback);

	void Write(std::string msg);
	
private:
	nlohmann::json call(int id, string method);
	nlohmann::json call(int id, string method, std::vector<string> params);
	nlohmann::json call(nlohmann::json data);

	void writeToSocket(std::string msg);
	void handleConnect(const boost::system::error_code& error, tcp::resolver::iterator endpoint_iterator);

	void threadAction();
	void readthreadAction();
};