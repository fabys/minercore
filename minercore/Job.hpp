#pragma once

#include <iostream>
#include <vector>
#include "json.hpp"

class Job
{
public:
	static Job* FromParams(nlohmann::json params);

private:
	std::string _id;
	std::string _version;
	std::string _prevHash;
	std::string _merklerRoot;
	std::string _time;
	std::string _bits;
	bool _cleanJobs;
};