#pragma once

#include "StratumClient.hpp";
#include "CPUSolver.hpp";

using namespace std;

class Miner
{
private:
	StratumClient *_client;
	CPUSolver *_solver;

public:
	Miner();

private: 
	void connectedCallback();
	void readCallback(nlohmann::json message);
};