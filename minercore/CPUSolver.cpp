#include "CPUSolver.hpp"
#include <iostream>
#include <sstream>
#include <bitset>
#include <string>


using namespace std;

CPUSolver::CPUSolver() {
}

void CPUSolver::SetNonce(std::string nonce) {
	string s = "0xA";
	stringstream ss;
	ss << nonce << s;
	unsigned n;
	ss >> n;
	bitset<32> b(n);
	
	_nonce1 = b.to_string();
}

void CPUSolver::SetTarget(std::string target) {
}

void CPUSolver::Run(Job * job) {

}
