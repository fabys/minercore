#include "Miner.hpp"
#include "json.hpp"
#include <boost/algorithm/string/predicate.hpp>
#include "Job.hpp";

using json = nlohmann::json;


Miner::Miner() {
	_solver = new CPUSolver();

	_client = new StratumClient();
	_client->Connect("eu1-zcash.flypool.org", "3333", std::bind(&Miner::connectedCallback, this));
}

void Miner::connectedCallback() {
	cout << "connected" << endl;
	json res;

	// subscribe
	res = _client->Subscribe();
	cout << res << endl;
	if(!res["error"].is_null()) {
		cout << "subscribe error: " << res["error"] << endl;
		return;
	}

	std::vector<string> params = res["result"].get<std::vector<string>>();
	_solver->SetNonce(params[0]);
	/*_nonce1 = params[0];
	_subscriptionId = params[1];*/


	// authorize
	res = _client->Authorize("t1cWBodffQvRHWPqHTHwxe3NgB4UYwtAqZU", "");
	cout << res << endl;
	if(!res["error"].is_null()) {
		cout << "subscribe error: " << res["error"] << endl;
		return;
	}

	// start read
	_client->Start(std::bind(&Miner::readCallback, this, std::placeholders::_1));
}

void Miner::readCallback(nlohmann::json message) {
	cout << "read callback: " << message << endl;

	if(message["method"] == "mining.set_target") {
		std::vector<string> params = message["params"].get<std::vector<string>>();
		_solver->SetTarget(params[0]);
		return;
	}

	if(message["method"] == "mining.notify") {
		Job *job = Job::FromParams(message["params"]);

		return;
	}


}