#include "StratumClient.hpp"
#include <boost/asio/ssl.hpp>

using json = nlohmann::json;

StratumClient::StratumClient(): _socket(_io_service)
{
}

void StratumClient::Connect(std::string const& host, std::string const& port, std::function<void()> callback) {
	_connectedCallback = callback;
	_connected = false;
	cout << "connecting" << endl;

	tcp::resolver r(_io_service);
	tcp::resolver::query q(host, port);
	tcp::resolver::iterator endpoint_iterator = r.resolve(q);
	tcp::resolver::iterator end;

	tcp::endpoint endpoint = *endpoint_iterator;
	_socket.async_connect(endpoint, boost::bind(&StratumClient::handleConnect, this, boost::asio::placeholders::error, ++endpoint_iterator));

	_workerThreads.create_thread(boost::bind(&StratumClient::threadAction, this));
}

json StratumClient::Subscribe() {
	return call(1, "mining.subscribe");
}

nlohmann::json StratumClient::Authorize(std::string const& username, std::string const& password) {
	return call(2, "mining.authorize", std::vector<string>{username, password});
}

void StratumClient::Start(std::function<void(nlohmann::json)> callback) {
	_readCallback = callback;
	_workerThreads.create_thread(boost::bind(&StratumClient::readthreadAction, this));
}

void StratumClient::writeToSocket(std::string msg) {
	cout << "write to socket " << msg << endl;
	boost::asio::streambuf write_buffer;
	std::ostream output(&write_buffer);
	output << msg;
	output << "\n";
		
	auto bytes_transferred = write_buffer.size();
	boost::asio::write(_socket, write_buffer, boost::asio::transfer_exactly(bytes_transferred));
	assert(write_buffer.size() == 0);
}

void StratumClient::handleConnect(const boost::system::error_code &error, tcp::resolver::iterator endpoint_iterator) {
	if(!error) {
		_connected = true;
		_connectedCallback();
	} else if(endpoint_iterator != tcp::resolver::iterator()) {
		_socket.close();
		tcp::endpoint endpoint = *endpoint_iterator;
		_socket.async_connect(endpoint, boost::bind(&StratumClient::handleConnect, this, boost::asio::placeholders::error, ++endpoint_iterator));
	}
}

json StratumClient::call(int id, string method) {
	json data;
	data["id"] = id;
	data["method"] = method;
	data["params"] = json::array();

	return call(data);
}

nlohmann::json StratumClient::call(int id, string method, std::vector<string> params) {
	json data;
	data["id"] = id;
	data["method"] = method;
	data["params"] = params;

	return call(data);
}

nlohmann::json StratumClient::call(nlohmann::json data) {
	writeToSocket(data.dump());

	boost::asio::streambuf responseBuffer;
	boost::asio::read_until(_socket, responseBuffer, "\n");

	std::istream is(&responseBuffer);
	std::string response;
	getline(is, response);

	cout << "call read: " << response << endl;

	std::stringstream ss;
	ss << response;
	json json;
	json << ss;
	return json;
}

void StratumClient::threadAction() {
	_io_service.run();
	assert(0);
}

void StratumClient::readthreadAction() {
	for(;;) {
		if(_socket.available()) {
			boost::asio::streambuf responseBuffer;
			boost::asio::read_until(_socket, responseBuffer, "\n");

			std::istream is(&responseBuffer);
			std::string response;
			getline(is, response);

			std::stringstream ss;
			ss << response;
			json json;
			json << ss;

			_readCallback(json);
		}
	}
}

void StratumClient::Write(std::string msg) {
	while(!_connected) {}
	writeToSocket(msg);
}
